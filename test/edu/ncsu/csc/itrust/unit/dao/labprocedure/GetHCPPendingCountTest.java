package edu.ncsu.csc.itrust.unit.dao.labprocedure;

import java.util.List;

import junit.framework.TestCase;
import edu.ncsu.csc.itrust.beans.LabProcedureBean;
import edu.ncsu.csc.itrust.beans.OfficeVisitBean;
import edu.ncsu.csc.itrust.dao.mysql.LabProcedureDAO;
import edu.ncsu.csc.itrust.dao.mysql.OfficeVisitDAO;
import edu.ncsu.csc.itrust.exception.DBException;
import edu.ncsu.csc.itrust.unit.datagenerators.TestDataGenerator;
import edu.ncsu.csc.itrust.unit.testutils.TestDAOFactory;

public class GetHCPPendingCountTest extends TestCase {
	private LabProcedureDAO lpDAO = TestDAOFactory.getTestInstance().getLabProcedureDAO();
    private OfficeVisitDAO ofDAO = TestDAOFactory.getTestInstance().getOfficeVisitDAO();

    private TestDataGenerator gen;
    
    private LabProcedureBean l1;
    private OfficeVisitBean o1;

	@Override
	protected void setUp() throws Exception {
		gen = new TestDataGenerator();
		gen.clearAllTables();
        gen.labProcedures();
        
        o1 = new OfficeVisitBean();
        o1.setHcpID(2333L);
        o1.setPatientID(3L);

		// first procedure
		l1 = new LabProcedureBean();
        l1.setPid(3L);
	//	l1.setOvID(919L);
        l1.setLoinc("10763-1");
        l1.setStatus(LabProcedureBean.Pending);
		
		l1.setCommentary("");
	}

	/**
	 * testGetAllLabProcedures
	 * @throws Exception
	 */
	public void testGetAllLabProcedures() throws Exception {
        long id2 = ofDAO.add(o1);
        l1.setOvID(id2);
        long id1 = lpDAO.addLabProcedure(l1);
        
		int pendingCount = lpDAO.getHCPPendingCount(2333L);
		assertEquals(1, pendingCount);
	}


	
}